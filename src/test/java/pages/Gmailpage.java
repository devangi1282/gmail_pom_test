package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;

/**
 * Created by admin on 10-07-2017.
 */
public class Gmailpage {

    WebDriver driver;
   /* @BeforeSuite
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "E:/selenium_driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://accounts.google.com/signin");
    }
*/

   //THIS IS ELEMENT FINDER
  //  By Email_id = By.id("identifierId");
   // By Password = By.name("password");
   // By Login = By.xpath("//*[@id=\"passwordNext\"]/content");


    @Test
    public Gmailpage(WebDriver driver){

        this.driver= driver;
    }
    @FindBy(id="identifierId")
    public  WebElement Email_id;

    @FindBy(name="password")
    public  WebElement Password;

    @FindBy(xpath="//*[@id=\"passwordNext\"]/content")
    public   WebElement Login;




    @Test
    public Gmailpage InputEmail_id(String email){

        Email_id.clear();
        Email_id.sendKeys(email);
        driver.findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span")).click();
        return this;

    }
    @Test
    public Gmailpage InputPassword(String pwd){

            Password.sendKeys(pwd);
            return this;
    }
    @Test
    public  void LoginbtnClick(){

        Login.click();
    }

    @Test
    public void login(String email,String pwd){

        //Fill user name

        this.InputEmail_id(email);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Fill password

        this.InputPassword(pwd);

        //Click Login button

        this.LoginbtnClick();

    }

}
