package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;

/**
 * Created by admin on 11-07-2017.
 */
public class SignUppage {

    WebDriver driver;


    public SignUppage(WebDriver driver){
        this.driver= driver;
    }

    @FindBy(name = "FirstName")
    public WebElement Name;

    @FindBy(name = "LastName")
    public  WebElement LastName;

    @FindBy(id = "GmailAddress")
    public WebElement EmailAddress;

    @FindBy(id = "Passwd")
    public WebElement Password;

    @FindBy(id = "PasswdAgain")
    public WebElement ConfirmPassword;

    @FindBy(xpath = "//*[@id=\"BirthMonth\"]/div")
    public WebElement BirthdayMonth;

    @FindBy(xpath = "//*[@id=\":6\"]/div")
    public WebElement SelectMonth;

    @FindBy(id = "BirthDay")
    public WebElement BirthdayDate;

    @FindBy(id = "BirthYear")
    public WebElement BirthdayYear;

    @FindBy(xpath = "//*[@id=\"Gender\"]/div[1]")
    public WebElement Gender;

    @FindBy(xpath = "//*[@id=\":e\"]")
    public WebElement Selectgender;

    @FindBy(id = "RecoveryPhoneNumber")
    public WebElement Phonenumber;

    @FindBy(id = "RecoveryEmailAddress")
    public WebElement Recoveryemail;

    @FindBy(id = "submitbutton")
    public WebElement Nextbtn;


    @Test
    public SignUppage InputFirstName(String name){

        Name.clear();
        Name.sendKeys(name);
        return this;

    }
    @Test
    public SignUppage InputLastname(String lstname){

        LastName.clear();
        LastName.sendKeys(lstname);
        return this;
    }
    @Test
    public SignUppage InputEmailaddress(String email){

        EmailAddress.clear();
        EmailAddress.sendKeys(email);
        return this;

    }
    @Test
    public SignUppage Inputpassword(String pwd){

        Password.clear();
        Password.sendKeys(pwd);
        return this;

    }

    @Test
    public SignUppage InputCnfrmpassword(String cnfmpwd){

        ConfirmPassword.clear();
        ConfirmPassword.sendKeys(cnfmpwd);
        return this;
    }

    @Test
    public SignUppage SelectBirthdayMonth(){

        //BirthdayMonth.clear();
        BirthdayMonth.click();
        return this;

    }
    @Test
    public SignUppage SelectBirthMonth(){

       // SelectMonth.clear();
       SelectMonth.click();
       return this;

    }
    @Test
    public SignUppage InputBirthdayDate(String bdydate){

        BirthdayDate.clear();
        BirthdayDate.sendKeys(bdydate);
        return this;
    }

    @Test
    public SignUppage InputBirthdayyear(String bdyyear){

       // BirthdayYear.clear();
        BirthdayYear.sendKeys(bdyyear);
        return this;
    }
    @Test
    public SignUppage Selectgender(){

        //Gender.clear();
       Gender.click();
        return this;
    }
    @Test
    public SignUppage Selectgendertype(){

     //  Selectgender.clear();
        Selectgender.click();
        return this;
    }
    @Test
    public SignUppage Inputphonenum(String phnnum){

       // Phonenumber.clear();
      Phonenumber.sendKeys(phnnum);
        return this;
    }
    @Test
    public SignUppage Inputrecoveryemail(String Reemail){

        // Phonenumber.clear();
        Recoveryemail.sendKeys(Reemail);
        return this;
    }

    @Test
    public void Nxtbtn(){


        Nextbtn.click();
    }
}
