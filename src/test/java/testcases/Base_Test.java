package testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import pages.Gmailpage;
import pages.SignUppage;

/**
 * Created by admin on 11-07-2017.
 */
public class Base_Test {

    protected WebDriver driver;

    public Gmailpage gmailpage;

    public SignUppage signUppage;

    @BeforeTest

    public void setup(){

        System.setProperty("webdriver.chrome.driver","E:/selenium_driver/chromedriver.exe");
        driver = new ChromeDriver();

        driver.get("https://accounts.google.com/SignUp");


        gmailpage = PageFactory.initElements(driver,Gmailpage.class);
        signUppage = PageFactory.initElements(driver,SignUppage.class);


    }

}
